var arrayinput = new Array(0);
console.log("array length", arrayinput.length);

function inputArray() {
  var number = document.getElementById("txt-number").value * 1;
  arrayinput.push(number);
  var contentHTML = "";
  //   console.log("array length", arrayinput.length);
  for (var i = 0; i < arrayinput.length; i++) {
    var numberArr = arrayinput[i];
    contentHTML += `<span class="mr-5">${numberArr}</span>`;
    // console.log({contentHTML});
  }
  document.getElementById("array_input").innerHTML = contentHTML;
}

function refreshResult(element) {
  // console.log("say yes");
  // console.log({element});
  document.getElementById(element).innerHTML = "";
}

function refreshResult1() {
  var elementName = document.querySelector("#result1").id;
  refreshResult(elementName);
}

function calculateSumPositiveNumber() {
  var sum = 0;

  for (var i = 0; i < arrayinput.length; i++) {
    var numberArr = arrayinput[i];
    if (numberArr > 0) {
      sum += numberArr;
    }
  }
  document.getElementById(
    "result1"
  ).innerHTML = `Tổng các phần tử dương trong mảng là ${sum}`;
}

function countPositiveNumber() {
  var count = 0;

  for (var i = 0; i < arrayinput.length; i++) {
    var numberArr = arrayinput[i];
    if (numberArr > 0) {
      count++;
    }
  }
  console.log("hello");
  document.getElementById(
    "result2"
  ).innerHTML = `Số phần tử dương trong mảng là: ${count}`;
}

function findTheLowestNumber() {
  var thelowest = arrayinput[0];

  for (var i = 1; i < arrayinput.length; i++) {
    if (thelowest > arrayinput[i]) {
      thelowest = arrayinput[i];
    }
  }
  console.log("ex3");
  if (thelowest == null) {
    document.getElementById(
      "result3"
    ).innerHTML = `Chưa có phần tử nào trong mảng`;
  } else {
    document.getElementById(
      "result3"
    ).innerHTML = `Phần tử nhỏ nhất trong mảng là: ${thelowest}`;
  }
}

function getPositiveArray(arrayInput) {
  var positiveArray = new Array(0);
  for (var i = 0; i < arrayInput.length; i++) {
    if (arrayInput[i] > 0) {
      positiveArray.push(arrayInput[i]);
    }
  }
  return positiveArray;
}

function findTheLowestPositiveNumber() {
  var positiveArray = getPositiveArray(arrayinput);
  var thelowest = positiveArray[0];
  for (var i = 1; i < positiveArray.length; i++) {
    if (thelowest > positiveArray[i]) {
      thelowest = positiveArray[i];
      console.log({ thelowest });
    }
  }
  if (thelowest == null) {
    document.getElementById(
      "result4"
    ).innerHTML = `Không có phần tử dương trong mảng`;
  } else {
    document.getElementById(
      "result4"
    ).innerHTML = `Phần tử nhỏ nhất trong mảng là: ${thelowest}`;
  }
}

function findEvenNumber() {
  console.log("I'm here");
  var index = -1;
  for (var i = 0; i < arrayinput.length; i++) {
    if (arrayinput[i] % 2 == 0 && arrayinput[i] != 0) {
      index = i;
    }
  }
  if (index == -1) {
    document.getElementById(
      "result5"
    ).innerHTML = `Không có phần tử chẵn trong mảng`;
  } else {
    document.getElementById(
      "result5"
    ).innerHTML = `Phần tử chẵn cuối cùng trong mảng là ${arrayinput[index]} `;
  }
}

function changePosition() {
  var posi1 = document.querySelector("#txt-pos1").value * 1;
  var posi2 = document.querySelector("#txt-pos2").value * 1;
  if (arrayinput[posi1] == null || arrayinput[posi2] == null) {
    document.getElementById(
      "result6"
    ).innerHTML = `Vị trí bạn nhập vào không tồn tại trong mảng`;
  } else {
    var temp = arrayinput[posi1];
    arrayinput[posi1] = arrayinput[posi2];
    arrayinput[posi2] = temp;
  }

  var contentHTML = "";
  //   console.log("array length", arrayinput.length);
  for (var i = 0; i < arrayinput.length; i++) {
    var numberArr = arrayinput[i];
    contentHTML += `<span class="mr-5">${numberArr}</span>`;
    // console.log({contentHTML});
  }
  document.querySelector(
    "#result6"
  ).innerHTML = `<span class="mr-3">Mảng sau khi đổi chỗ:</span> ${contentHTML}`;
}

function sortArray() {
  var sortArray = arrayinput.slice();
  sortArray.sort(function (a, b) {
    return a - b;
  });
  document.getElementById("result7").innerHTML = sortArray;
  console.log("sortArray: " + sortArray);
  console.log("arrayinput: ", arrayinput);
}

function isPrime(element) {
  if (element == 0 || element == 1) {
    return false;
  } else if (element == 2) {
    return true;
  } else {
    for (var i = 2; i < element; i++) {
      if (element % i == 0) {
        return false;
      }
    }
    return true;
  }
}

function findPrime() {
  var prime = arrayinput.find((element) => isPrime(element));
  console.log({ prime });
  if (prime == null) {
    document.getElementById(
      "result8"
    ).innerHTML = `Mảng không có số nguyên tố`;
  } else {
    document.getElementById(
      "result8"
    ).innerHTML = `Số nguyên tố đầu tiên trong mảng là: ${prime}`;
  }
}

function countInteger(){
  var count = 0 ;
  var contentHTML ="";
  arrayinput.forEach( function (element){
    if(Number.isInteger(element)){
      contentHTML += `<span class="mr-3"> ${element}</span>`
    }
  });
  if(contentHTML==""){
    document.getElementById("result9").innerHTML = "Không có số nguyên nào trong mảng";
  }else{
    document.getElementById("result9").innerHTML = `Các số nguyên trong mảng là ${contentHTML}`;
  }

  console.log({count});
}

function compareNegativeAndPositive(){
  var negativeArray = arrayinput.filter(element=>element<0);
  var positiveArray = arrayinput.filter(element=>element>0)
  document.getElementById(
    "result10"
  ).innerHTML = `Số số âm trong mảng là ${negativeArray.length} và số số dương trong mảng là ${positiveArray.length}`;
}
